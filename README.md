# ProtobufSaveLoadData_Practice

* 下載protobuf-net - https://github.com/mgravell/protobuf-net
* 將protobuf-net.dll加到專案中
* 基本用法
1. 加上裝飾符，注意ProtoMember(tag) tag不重複
```
[ProtoContract]
public class TestData
{

    [ProtoMember(1)]
    public int[] index;

    [ProtoMember(2)]
    public string[] content;
}
```
2. Serialize Data
```
using (Stream file = File.Create(FilePath))
{
    Serializer.Serialize<TestData>(file, datas);
    file.Close();
}   
```
3. Deserialize Data
```
using (Stream file = File.OpenRead(FilePath))
{
    datas = Serializer.Deserialize<TestData>(file);
    file.Close();
}
```
