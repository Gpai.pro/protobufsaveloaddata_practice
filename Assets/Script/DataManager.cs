﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ProtoBuf;
using System;
using System.IO;

public class DataManager : MonoBehaviour
{
    public string FilePath;     // save file path
    public MemberData member;

    private TestData datas;
    private static string savePrefix = "saveData";  //  PlayerPrefs save key

    void Start()
    {
       
    }

    private void Awake()
    {
        datas = new TestData();
    }

    // 根據FilePath存成.proto檔
    public void SaveDataToFile()
    {
        if (string.IsNullOrEmpty(FilePath))
        {
            Debug.Log("FilePath is Null");
            return;
        }

        if (datas == null)
        {
            Debug.Log("Object is null");
            return;
        }
        datas.index = member.indexs;
        datas.content = member.contents;

        //using (FileStream stream = new FileStream(FilePath, FileMode.Create, FileAccess.Write))
        //{
        //    Serializer.Serialize<TestData>(stream, datas);
        //    stream.Flush();
        //} 
        using (Stream file = File.Create(FilePath))
        {
            Serializer.Serialize<TestData>(file, datas);
            file.Close();
        }                   
    }
    
    public void LoadDataToFile()
    {
        if (string.IsNullOrEmpty(FilePath))
        {
            Debug.Log("FilePath is Null");
            return;
        }

        if (!File.Exists(FilePath))
        {
            Debug.Log("FilePaht not exists");
            return;
        }

        //FileStream stream = new FileStream(FilePath, FileMode.Open, FileAccess.Read);
        //datas = Serializer.Deserialize<TestData>(stream);
        using (Stream file = File.OpenRead(FilePath))
        {
            datas = Serializer.Deserialize<TestData>(file);
            file.Close();
        }

        member.indexs = datas.index;
        member.contents = datas.content;        
    }

    // 轉成string後使用PlayerPrefs存讀
    public void SaveDataToPlayerPrefs()
    {
        if (datas == null)
        {
            Debug.Log("Object is null");
            return;
        }
        datas.index = member.indexs;
        datas.content = member.contents;

        MemoryStream stream = new MemoryStream();
        Serializer.Serialize<TestData>(stream, datas);
        string textBase64 = Convert.ToBase64String(stream.ToArray());
        PlayerPrefs.SetString(savePrefix, textBase64);
    }

    public void LoadDataToPlayerPrefs()
    {
        string str = PlayerPrefs.GetString(savePrefix);
        byte[] bytes = Convert.FromBase64String(str);
        MemoryStream stream = new MemoryStream(bytes);

        datas = Serializer.Deserialize<TestData>(stream);
        member.indexs = datas.index;
        member.contents = datas.content;
    }
}