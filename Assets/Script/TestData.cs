﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ProtoBuf;

//[ProtoContract]
//public class TestDatas
//{
//    [ProtoMember(1)]
//    public TestData[] datas;
//}

[ProtoContract]
public class TestData
{

    [ProtoMember(1)]
    public int[] index;

    [ProtoMember(2)]
    public string[] content;
}
